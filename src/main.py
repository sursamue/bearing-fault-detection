from utils import *
from pathlib import Path
from tqdm.auto import tqdm
from torch.utils.data import DataLoader#,WeightedRandomSampler
from torch import nn
from torch.optim import Adam
from torch.nn import NLLLoss
import models as models
from copy import deepcopy



# {'N':0, 'B':1, 'IR':2, 'OR':3}


def main():
    args = read_args()
    # set of global random shuffle
    np.random.seed(2001)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print("device: ", device)

    if args.purge:
        purge_cache()
        quit()
    if args.load:
        # check if cache empty
        if not is_cache_empty():
            warning("Cache not empty, please purge cache before loading new data!")
            quit()
        old_df = load_df(Path('/home/samuel/Documents/CVUT/mgr/semester_1/DIT/semestralka/src/df_1024.csv'))
        # remove all files containing 007 in their name
        df = deepcopy(old_df[~old_df['filename'].str.contains('007')]).reset_index(drop=True)
        
        run_and_save_wavelet_transform(df, device=device, length_of_samples=args.length_of_samples)
        df = load_df(Path('/home/samuel/Documents/CVUT/mgr/semester_1/DIT/semestralka/src/cache/df.csv'))
        train_df, _, _ = split_df_ver2(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42)
        # train_loader = DataLoader(dataset=train_set, batch_size = 10, shuffle=False, num_workers=4)
        # get mean and std
        mean, std = dataset_mean_std_from_df(train_df)
        print("Mean and std computed!")
        # save mean and std to cache
        save_mean_std(mean, std)
        quit()
    if args.purge_load:
        purge_cache()
        old_df = load_df(Path('/home/samuel/Documents/CVUT/mgr/semester_1/DIT/semestralka/src/df_1024.csv'))
        # remove all files containing 007 in their name
        df = deepcopy(old_df[~old_df['filename'].str.contains('007')]).reset_index(drop=True)
        run_and_save_wavelet_transform(df, device=device, length_of_samples=args.length_of_samples)
        df = load_df(Path('/home/samuel/Documents/CVUT/mgr/semester_1/DIT/semestralka/src/cache/df.csv'))
        
        train_df, _, _ = split_df_ver2(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42)
        # train_loader = DataLoader(dataset=train_set, batch_size = 10, shuffle=False, num_workers=4)
        # get mean and std
        mean, std = dataset_mean_std_from_df(train_df)
        print("Mean and std computed!")
        # save mean and std to cache
        save_mean_std(mean, std)
        quit()
    if args.purge_load2:
        purge_cache()
        old_df = load_df(Path('./df_1024.csv'))
        # remove all files containing 007 in their name
        df = deepcopy(old_df[~old_df['filename'].str.contains('007')]).reset_index(drop=True)
        run_and_save_wavelet_transform2(df, device=device, length_of_samples=args.length_of_samples)
        df = load_df(Path('./df.csv'))
        
        train_df, _, _ = split_df_ver2(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42)
        # train_loader = DataLoader(dataset=train_set, batch_size = 10, shuffle=False, num_workers=4)
        # get mean and std
        mean, std = dataset_mean_std_from_df3(train_df)
        print("Mean and std computed!")
        # save mean and std to cache
        save_mean_std(mean, std)
        quit()
    
   
    
        
    if args.train:
    
        # load df
        df = load_df(Path('./cache/df.csv'))


        #get all of the labels and their counts in df[:,0] 
        labels, counts = np.unique(df.iloc[:,0], return_counts=True)
        label_dict ={'N':0, 'B':1, 'IR':2, 'OR':3}
        # map labels from numbers to strings
        labels = [list(label_dict.keys())[list(label_dict.values()).index(i)] for i in labels]
        print(labels, counts)
        
        # input()

        # split df 
        train_df, val_df, test_df = split_df_ver2(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42)
        labels, counts = np.unique(train_df.iloc[:,0], return_counts=True)
        labels = [list(label_dict.keys())[list(label_dict.values()).index(i)] for i in labels]
        print(f"train labels: {labels}, \ntrain counts: {counts}")

        labels, counts = np.unique(val_df.iloc[:,0], return_counts=True)
        labels = [list(label_dict.keys())[list(label_dict.values()).index(i)] for i in labels]
        print(f"val labels: {labels}, \nval counts: {counts}")

        labels, counts = np.unique(test_df.iloc[:,0], return_counts=True)
        labels = [list(label_dict.keys())[list(label_dict.values()).index(i)] for i in labels]
        print(f"test labels: {labels}, \ntest counts: {counts}")





        mean, std = load_mean_std()

        train_ds = ScatteringDataset(df = train_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)
        val_ds = ScatteringDataset(df = val_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)
        test_ds = ScatteringDataset(df = test_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)

        if args.full:
            train_ds = ScatteringDatasetFull(df = train_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)
            val_ds = ScatteringDatasetFull(df = val_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)
            test_ds = ScatteringDatasetFull(df = test_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)

        # create x_all and y_all
        # x_all = []
        # y_all = []
        # Sx_all = pd.DataFrame()



        # for i in tqdm(range(len(train_ds))):
        #     x, Sx, y = train_ds[i]
        #     print(Sx)
        #     input()
        #     # Sx to file
            


        #     input()
        #     x_all.append(x)
        #     y_all.append(y)
        #     Sx_all.append(Sx)
        # x_all = torch.stack(x_all)
        # y_all = torch.stack(y_all)
        # # Sx_all = torch.stack(Sx_all)

        
        # print(x_all.shape)
        # print(y_all.shape)
        # print(x_all)
        # # print largest value in x_all
        # print(torch.max(x_all))

        
        train_loader = DataLoader(dataset=train_ds, batch_size = 32, shuffle=False, num_workers=4)
        val_loader = DataLoader(dataset=val_ds, batch_size = 32, shuffle=False, num_workers=4)
        test_loader = DataLoader(dataset=test_ds, batch_size = 32, shuffle=False, num_workers=4)
        



        # mean, std = dataset_mean_std(train_loader) # TODO: THIS to the global variable, do not run this all the time
        # print("mean and std: \n", mean, std)
        # input()
    

        # model = nn.Sequential(nn.Linear(221, 4), nn.LogSoftmax(dim=1))
        model = models.Net8(input_size=221, output_size=4)
        logger = metricsLogger(model_name = model.__class__.__name__)
        # model = models.Net4(input_size=3536, output_size=4)
        model = train_model(model, train_loader, val_loader, epochs=10, lr=1e-3, device=device, logger = logger)

    if args.test:
        # load df
        df = load_df(Path('./cache/df.csv'))

        # split df
        _, _, test_df = split_df_ver2(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42)
        # load mean and std
        mean, std = load_mean_std()
        # create test dataset
        test_ds = ScatteringDataset(df = test_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)
        if args.full:
            test_ds = ScatteringDatasetFull(df = test_df, do_scattering = True, normalize = True, mean = mean, std = std, length_of_samples=args.length_of_samples)
        # create test loader
        test_loader = DataLoader(dataset=test_ds, batch_size = 32, shuffle=False, num_workers=4)
        # load model
        model = models.Net8(input_size=221, output_size=4)
        model.load_state_dict(torch.load(Path(args.model_path)))
        # test model


        y_true, y_pred = test_model(model, test_loader, device=device)
        test_logger = metricsLogger(model_name = model.__class__.__name__)
        labels_dict = {'N':0, 'B':1, 'IR':2, 'OR':3}
        test_logger.save_confusion_matrix(y_true, y_pred, labels = labels_dict)
        

    
    

main()