import pandas
import argparse
from pathlib import Path


# import plot utils
from plot_utils import *

def main():
    args = parse_args()
    if args.dataframe:
        print("Plotting metrics from dataframe: ", args.dataframe)
        df = pandas.read_csv(args.dataframe)
        plot_metrics(df)
    if args.net:
        print("Plotting net")
        plot_net()
    if args.histogram:
        print("Plotting histogram")
        path = Path('cache/df.csv')
        plot_histogram(path)







main()