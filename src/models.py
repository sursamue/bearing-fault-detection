
import torch
import torch
import torch
import torch
import torch.nn as nn

# architecture
class Net1(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net1, self).__init__()
        self.fc1 = nn.Linear(input_size, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64,output_size)
        self.relu = nn.ReLU()
        self.log_softmax = nn.LogSoftmax(dim=1)
        self.double()
    
    def forward(self, x):
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x= self.relu(x)
        x = self.fc3(x)
        x = self.log_softmax(x)
        return x
    
    def predict(self, x):
        x = self.forward(x)
        return torch.argmax(x, dim=1)
    
    def predict_proba(self, x):
        x = self.forward(x)
        return torch.exp(x)
    
    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(torch.load(path))


# architecture with maxpool and batchnorm and dropout
class Net2(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net2, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Linear(input_size, 128),
            nn.ReLU(),
            # nn.BatchNorm1d(128),
        )
        self.layer2 = nn.Sequential(
            nn.Linear(128, 64),
            nn.ReLU(),
            
            # nn.BatchNorm1d(64), 
        )
        self.layer3 = nn.Sequential(
            nn.Linear(64, output_size),
            nn.LogSoftmax(dim=1)
        )
        self.double()

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)        
        return x
    
    def predict(self, x):
        x = self.forward(x)
        return torch.argmax(x, dim=1)
    
    def predict_proba(self, x):
        x = self.forward(x)
        return torch.exp(x)
    
    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(torch.load(path))

    

# architecture for input size 1768x1
class Net3(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net3, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Linear(input_size, 512),
            nn.ReLU(),
            nn.BatchNorm1d(512),
        )
        self.layer2 = nn.Sequential(
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.BatchNorm1d(256), 
        )
        self.layer3 = nn.Sequential(
            nn.Linear(256, output_size),
            nn.LogSoftmax(dim=1)
        )
        self.double()

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)        
        return x
    
    def predict(self, x):
        x = self.forward(x)
        return torch.argmax(x, dim=1)
    
    def predict_proba(self, x):
        x = self.forward(x)
        return torch.exp(x)
    
    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(torch.load(path))
        



import torch.nn as nn

# new architecture
class Net4(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net4, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Linear(input_size, 1024),
            nn.ReLU(),
            nn.BatchNorm1d(1024),
        )
        self.layer2 = nn.Sequential(
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.BatchNorm1d(512),
        )
        self.layer3 = nn.Sequential(
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.BatchNorm1d(256),
        )
        self.layer4 = nn.Sequential(
            nn.Linear(256, output_size),
            nn.LogSoftmax(dim=1)
        )
        self.double()

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        return x
    
    def predict(self, x):
        x = self.forward(x)
        return torch.argmax(x, dim=1)
    
    def predict_proba(self, x):
        x = self.forward(x)
        return torch.exp(x)
    
    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(torch.load(path))



# create a model with 2 hidden layers using nn.Sequential use maxpool, dropout, but not batchnorm
class Net5(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net5, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Linear(input_size, 128),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer2 = nn.Sequential(
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer3 = nn.Sequential(
            nn.Linear(64, output_size),
            nn.LogSoftmax(dim=1)
        )
        self.double()

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)        
        return x
    
    def predict(self, x):
        x = self.forward(x)
        return torch.argmax(x, dim=1)
    
    def predict_proba(self, x):
        x = self.forward(x)
        return torch.exp(x)
    
    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(torch.load(path))


# same as net 5, but try conv layers
class Net6(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net6, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv1d(221, 8, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer2 = nn.Sequential(
            nn.Conv1d(8, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer3 = nn.Sequential(
            nn.Conv1d(16, 32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer4 = nn.Sequential(
            nn.Conv1d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer5 = nn.Sequential(
            nn.Conv1d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer6 = nn.Sequential(
            nn.Conv1d(128, 256, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer7 = nn.Sequential(
            nn.Conv1d(256, 512, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer8 = nn.Sequential(
            nn.Conv1d(512, 1024, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Dropout(p=0.2),
        )
        self.layer9 = nn.Sequential(
            nn.Linear(16384, output_size),
            nn.LogSoftmax(dim=1)
        )
        self.double()

    def forward(self, x):
        x = self.layer1(x)
        # print("layer 1")    
        # print(x.shape)
        # input()
        x = self.layer2(x)
        # print("layer 2")
        # print(x.shape)
        # input()
        x = self.layer3(x)  
        # print("layer 3")
        # print(x.shape)
        # input()     
        x = self.layer4(x)   
        # print("layer 4")
        # print(x.shape)
        # input()     
        x = self.layer5(x)  
        # print("layer 5")
        # print(x.shape)
        # input()      
        x = self.layer6(x)   
        # print("layer 6")
        # print(x.shape)
        # input()     
        x = self.layer7(x)
        # print("layer 7")
        # print(x.shape)
        # input()
        x = self.layer8(x)
        # print("layer 8")
        # print(x.shape)
        # input()
        x = x.view(x.size(0), -1) # x. view means flatten
        # print("view")
        # print(x.shape)
        # input()
        x = self.layer9(x)
        # print("layer 9")
        # print(x.shape)
        # input()
        return x



# create a model with 2 hidden layers using nn.Sequential with maxpool, dropout, but not batchnorm
class Net7(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net7, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Linear(input_size, 128),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.Dropout(0.2)
        )
        self.layer2 = nn.Sequential(
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.Dropout(0.2)
        )
        self.fc = nn.Linear(32, output_size)  # Update the input size to 16
        self.softmax = nn.LogSoftmax(dim=1)
        self.double()

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = x.view(x.size(0), -1) # x. view means flatten
        x = self.fc(x)
        x = self.softmax(x)
        return x

    def predict(self, x):
        # TODO: Implement prediction logic
        pass

    def predict_proba(self, x):
        # TODO: Implement prediction probability logic
        pass

    def save(self, path):
        # TODO: Implement save model logic
        pass

    def load(self, path):
        # TODO: Implement load model logic
        pass


# create NET with convolution linear, maxpool, dropout, the input is 221x16  but only few convoutions
class Net8(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net8, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv1d(221, 8, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.Dropout(0.2)
        )
        self.layer2 = nn.Sequential(
            nn.Conv1d(8, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.Dropout(0.2)
        )
        self.layer3 = nn.Sequential(
            nn.Conv1d(16, 32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.Dropout(0.2)
        )
        self.layer4 = nn.Sequential(
            nn.Conv1d(32, 64,
                       kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.Dropout(0.2)
        )
        self.fc = nn.Linear(64, output_size)  # Update the input size to 16
        self.softmax = nn.LogSoftmax(dim=1)
        self.double()

    def forward(self, x):
        # print("Input shape:", x.shape)
        x = self.layer1(x)
        # print("After layer1 shape:", x.shape)
        # input()
        x = self.layer2(x)
        # print("After layer2 shape:", x.shape)
        # input()
        x = self.layer3(x)
        # print("After layer3 shape:", x.shape)
        # input()
        x = self.layer4(x)
        # print("After layer4 shape:", x.shape)
        # input()
        x = x.view(x.size(0), -1) # x. view means flatten
        # print("After flatten shape:", x.shape)
        # input()
        x = self.fc(x)
        # print("After fc shape:", x.shape)
        # input()
        x = self.softmax(x)
        # print("Output shape:", x.shape)
        # input()
        return x

    def predict(self, x):
        #todo:
        pass
