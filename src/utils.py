# {'N':0, 'B':1, 'IR':2, 'OR':3}
import argparse

import torch
import torch.nn as nn

from torch.optim import Adam
from torch.utils.data import DataLoader
from pathlib import Path
import pandas as pd
import kymatio
from kymatio.numpy import Scattering1D
from copy import deepcopy

from tqdm import tqdm
import time
# import datetime
from datetime import datetime
# sns
import seaborn as sns



import os
import numpy as np

from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

from kymatio.torch import Scattering1D
import numpy as np


# import balanced accuracy score
from sklearn.metrics import balanced_accuracy_score

# Load the dataset
def load_df(path: Path) -> pd.DataFrame:
    df = pd.read_csv(path)
    return df


class ScatteringDatasetFull(torch.utils.data.Dataset):
    def __init__(self, df, mean, std, has_label = True, do_scattering = True, transform=None, normalize = True, length_of_samples = 512):
        self.df = df
        self.transform = transform
        self.has_label = has_label
        self.do_scattering = do_scattering # this should be always true
        self.log_eps = 1e-6 # for numerical stability
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.normalize = normalize
        self.length_of_samples = length_of_samples # len of every sample
        # TODO: self.mean and self.std should be calculated from the training set only
        # please recalculate them when you change the training set
        # currently calculated just for the first 100 samples from the df
        self.mean = mean
        self.std = std

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        Sx = None
        x = self.df.iloc[index, 2:self.length_of_samples+2].values # signal
        Sx = self.df.iloc[index, self.length_of_samples+2:].values # TODO: toto je zle
        x = x.astype(np.float32)    
        Sx = Sx.astype(np.float32)   
        
        
        



        x = x/np.max(x)
        x = torch.from_numpy(x)
        Sx = torch.from_numpy(Sx)
        

        if self.do_scattering: # TODO: toto zmazat , lebo tento if je zbytocny
            # torch.set_printoptions(threshold=10000)
            # get length of signal

            # from the documentation:
            # the output is in the form of a torch.Tensor or size (B, C, N1), where N1 is the signal length after subsampling to the scale (with the appropriate oversampling factor to reduce aliasing), and C is the number of scattering coefficients. 
            
            # Sx is 2d remove zeroth order as it does not contain any information

            # -----------------------
            # THIS CODE NOT NEEDED AS THE Sx IS ALREADY read from the df
            # T = 512 # x.shape[-1]
            # J = 6
            # Q = 16

            # scattering = Scattering1D(J, T, Q, out_type = 'array').to(self.device)
            # Sx = scattering(x)
            # Sx = Sx[1:, :]     

        
        
            # Sx = torch.log(torch.abs(Sx) + self.log_eps) # log + epsilon for numerical stability
            # Sx = torch.mean(Sx, dim=-1) # average over time
            # ------------------------
            if self.normalize:
                Sx = (Sx - self.mean)/self.std
                # unflatten from 1xN to 221x16
                Sx = Sx.reshape(221,16)
                
            
        

        if self.has_label:
            y = self.df.iloc[index, 0] # label
            y = torch.tensor(y, dtype=torch.long)
            return x, Sx, y
        else:
            return x, Sx, -1


    
class SaveScatteringDataset(torch.utils.data.Dataset):
    def __init__(self, df, has_label = True, length_of_samples = 512):
        self.df = df
        self.has_label = has_label
        self.length_of_samples = length_of_samples # len of every sample

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        x = self.df.iloc[index, 2:self.length_of_samples+2].values 
        x = x.astype(np.float32)

        x = x/np.max(x)
        
        # x = np.divide(x, np.max(np.abs(x)))
        x = torch.from_numpy(x)
        # x to torch


        

         
        if self.has_label:
            y = self.df.iloc[index, 0] # label
            y = torch.tensor(y, dtype=torch.long)
            return x, y
        else:
            return x, -1
        

class SaveScatteringDatasetFull(torch.utils.data.Dataset):
    def __init__(self, df, has_label = True, length_of_samples = 512):
        self.df = df
        self.has_label = has_label
        self.length_of_samples = length_of_samples # len of every sample

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        x = self.df.iloc[index, 2:self.length_of_samples+2].values 
        x = x.astype(np.float32)

        x = x/np.max(x)
        
        # x = np.divide(x, np.max(np.abs(x)))
        x = torch.from_numpy(x)
        # x to torch


        

         
        if self.has_label:
            y = self.df.iloc[index, 0] # label
            y = torch.tensor(y, dtype=torch.long)
            return x, y
        else:
            return x, -1



class ScatteringDataset(torch.utils.data.Dataset):
    def __init__(self, df, mean, std, has_label = True, do_scattering = True, transform=None, normalize = True, length_of_samples = 512):
        self.df = df
        self.transform = transform
        self.has_label = has_label
        self.do_scattering = do_scattering # this should be always true
        self.log_eps = 1e-6 # for numerical stability
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.normalize = normalize
        self.length_of_samples = length_of_samples # len of every sample
        # TODO: self.mean and self.std should be calculated from the training set only
        # please recalculate them when you change the training set
        # currently calculated just for the first 100 samples from the df
        self.mean = mean
        self.std = std

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        Sx = None
        x = self.df.iloc[index, 2:self.length_of_samples+2].values # signal
        Sx = self.df.iloc[index, self.length_of_samples+2:].values
        x = x.astype(np.float32)    
        Sx = Sx.astype(np.float32)   

        x = x/np.max(x)
        x = torch.from_numpy(x)
        Sx = torch.from_numpy(Sx)

        if self.do_scattering: # TODO: toto zmazat , lebo tento if je zbytocny
            torch.set_printoptions(threshold=10000)
            # get length of signal

            # from the documentation:
            # the output is in the form of a torch.Tensor or size (B, C, N1), where N1 is the signal length after subsampling to the scale (with the appropriate oversampling factor to reduce aliasing), and C is the number of scattering coefficients. 
            
            # Sx is 2d remove zeroth order as it does not contain any information

            # -----------------------
            # THIS CODE NOT NEEDED AS THE Sx IS ALREADY read from the df
            # T = 512 # x.shape[-1]
            # J = 6
            # Q = 16

            # scattering = Scattering1D(J, T, Q, out_type = 'array').to(self.device)
            # Sx = scattering(x)
            # Sx = Sx[1:, :]     

        
        
            # Sx = torch.log(torch.abs(Sx) + self.log_eps) # log + epsilon for numerical stability
            # Sx = torch.mean(Sx, dim=-1) # average over time
            # ------------------------
            if self.normalize:
                Sx = (Sx - self.mean)/self.std
                
            
        

        if self.has_label:
            y = self.df.iloc[index, 0] # label
            y = torch.tensor(y, dtype=torch.long)
            return x, Sx, y
        else:
            return x, Sx, -1
        
def split_df(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42):
    """
    Function to split the dataframe into train, validation and test dataframes.
    :param df: dataframe to split
    :param train_size: size of training set
    :param val_size: size of validation set
    :param test_size: size of test set
    :param random_state: random state for reproducibility
    :return: train, validation and test dataframes
    """

    assert train_size + val_size + test_size == 1

    train, val, test = \
        np.split(df.sample(frac=1, random_state=random_state), 
                [int(train_size*len(df)), int((train_size+val_size)*len(df))])
    return train, val, test

def split_df_ver2(df, train_size=0.6, val_size=0.2, test_size=0.2, random_state=42): # delete random state as np random state is being used
    """
    Same as split_df, but rows with same "filename" will be treated as one unit.
    """
    assert train_size + val_size + test_size == 1
    # get unique filenames
    filenames = df['filename'].unique()
    # shuffle filenames
    np.random.shuffle(filenames)
    # get number of filenames
    nb_filenames = len(filenames)
    # get number of train, val and test filenames
    nb_train_filenames = int(train_size * nb_filenames)
    nb_val_filenames = int(val_size * nb_filenames)
    nb_test_filenames = int(test_size * nb_filenames)
    # get train, val and test filenames
    train_filenames = filenames[:nb_train_filenames]
    val_filenames = filenames[nb_train_filenames:nb_train_filenames+nb_val_filenames]
    test_filenames = filenames[nb_train_filenames+nb_val_filenames:]
    # get train, val and test dataframes
    train = deepcopy(df[df['filename'].isin(train_filenames)]).reset_index(drop=True)
    val = deepcopy(df[df['filename'].isin(val_filenames)]).reset_index(drop=True)
    test = deepcopy(df[df['filename'].isin(test_filenames)]).reset_index(drop=True)

    return train, val, test




def dataset_mean_std(loader):
    """
    Compute the mean and std value of dataset.
    :param loader: the training loader created by `DataLoader` from your dataset,
                    it should be an instance of `torch.utils.data.DataLoader`
                    and it should return 3 values in each iteration,
                    x, Sx, y
    :return: (mean, std) tuple of mean and std.
    """
    mean = 0.
    std = 0.
    nb_samples = 0.
    for data in tqdm(loader):
        data = data[1]
        batch_samples = data.size(0) # len of batch
        # data to numpy
        data = data.numpy()
        # data to tensor
        data = torch.from_numpy(data)
        # compute mean and std
        mean += torch.mean(data, dim=(0))
        std += torch.std(data, dim=(0))
        nb_samples += batch_samples

    mean /= nb_samples
    std /= nb_samples

def load_mean_std(path:Path = Path('cache')):
    """
    Load mean and std from the given path.
    :param path: path to load
    :return: (mean, std) tuple of mean and std
    """
    assert os.path.exists(path), 'Path does not exist!'
    # numpy read from txt
    mean = np.loadtxt(os.path.join(path, 'mean.txt'))
    std = np.loadtxt(os.path.join(path, 'std.txt'))
    
    
    # mean and std to tensor
    mean = torch.from_numpy(np.array(mean))
    std = torch.from_numpy(np.array(std))
    return mean, std


def dataset_mean_std_from_df3(train_df):
    """
    Compute mean and std from train dataframe.
    The dataframe must have a columns named "Sx_0, Sx_1, ..., Sx_N" where N is the number of scattering coefficients.
    :param train_df: train dataframe
    :return: (mean, std) tuple of mean and std
    """
    mean_list = np.array([])
    std_list = np.array([])
    nb_samples = 0
    
    # get sub df with only Sx columns
    cols = [col for col in train_df.columns if 'Sx' in col]
    cols_df = train_df[cols]
    # cols_df to numpy
    cols_numpy = cols_df.to_numpy()
    # to tensor
    cols_tensor = torch.from_numpy(cols_numpy)

    print("cols: ", cols_tensor)
    print("cols.shape: ", cols_tensor.shape)
    input()
    mean_tensor = torch.mean(cols_tensor, dim=0)
    std_tensor = torch.std(cols_tensor, dim=0) # david chytil std 
    # for col in cols:
    #     mean_list = np.append(mean_list, train_df[col].mean())
    #     std_list = np.append(std_list, train_df[col].std())

    # mean and std to numpy arrays
    mean_list = mean_tensor.numpy()
    std_list = std_tensor.numpy()
   
    return mean_list, std_list


def dataset_mean_std_from_df(train_df):
    """
    Compute mean and std from train dataframe.
    The dataframe must have a columns named "Sx_0, Sx_1, ..., Sx_N" where N is the number of scattering coefficients.
    :param train_df: train dataframe
    :return: (mean, std) tuple of mean and std
    """
    mean_list = np.array([])
    std_list = np.array([])
    nb_samples = 0
    
    # get sub df with only Sx columns
    cols = [col for col in train_df.columns if 'Sx' in col]
    cols_df = train_df[cols]
    # cols_df to numpy
    cols_numpy = cols_df.to_numpy()
    # to tensor
    cols_tensor = torch.from_numpy(cols_numpy)

    print("cols: ", cols_tensor)
    print("cols.shape: ", cols_tensor.shape)
    input()
    mean_tensor = torch.mean(cols_tensor, dim=0)
    std_tensor = torch.std(cols_tensor, dim=0) # david chytil std 
    # for col in cols:
    #     mean_list = np.append(mean_list, train_df[col].mean())
    #     std_list = np.append(std_list, train_df[col].std())

    # mean and std to numpy arrays
    mean_list = mean_tensor.numpy()
    std_list = std_tensor.numpy()
   
    return mean_list, std_list



def is_cache_empty(path = Path('cache')):
    """
    Check if the cache folder is empty.
    :param path: path to cache folder
    :return: True if empty, False otherwise
    """
    if not os.path.exists(path):
        return True
    else:
        if not os.listdir(path):
            return True
        else:
            return False


def warning(msg):
    """
    Print the message as a WARNING message.
    :param msg: message to print
    :return: None
    """
    print('\033[93m' + msg + '\033[0m')


def save_mean_std(mean: np.array, std:np.array, path:Path = Path('cache')):
    """
    Save the mean and std value to the given path.
    :param mean: mean value
    :param std: std value
    :param path: path to save
    :return: None
    """
    # numpy savetxt
    np.savetxt(os.path.join(path, 'mean.txt'), mean)
    np.savetxt(os.path.join(path, 'std.txt'), std)

    print('Mean and std saved to {}.'.format(path))


def mean_std(loader):
    x, Sx, lebels = next(iter(loader))
    print(Sx.shape)
    mean = Sx.mean(dim=0)
    std = Sx.std(dim=0)
  # shape of images = [b,c,w,h]
#   mean, std = torch.mean
    return mean, std 

def purge_cache():
    """
    This function will delete everything inside the 'cache' folder
    :return: None
    """
    folder = 'cache'
    # check if 'cache' folder is empty
    if not os.listdir(folder):
        print('Cache is already empty!')
        return
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path): # if file or link
                os.unlink(file_path)
            elif os.path.isdir(file_path): # if directory
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))
     
    print('Cache purged successfully!')


def read_args():
    """
    This function will read arguments from command line
    :return: args
    """
    parser = argparse.ArgumentParser(description='Bearing fault detection. \nExample usage: $ python3 main.py -full -len 1024 -test -model models/Net8_20240103_0025/Net8_20240103_0025.pt')
    parser.add_argument('-p', '--purge', help='Will delete the temp folder and run program', nargs='?', const=True, default=False)
    parser.add_argument('-l', '--load', help='Load the dishwas...Do the scattering transform and save it to the dataframe', nargs='?', const=True, default=False)
    parser.add_argument('-pl', '--purge_load', help='Purge the cache and load the dishwas...Do the scattering transform and save it to the dataframe', nargs='?', const=True, default=False)
    parser.add_argument('-pl2', '--purge_load2', help='Purge the cache and load the dishwas...Do the scattering transform and save it to the dataframe', nargs='?', const=True, default=False)
    parser.add_argument('-full', '--full', help='Use full images', nargs='?', const=True, default=False) # nargs = '?' means that the argument is optional
    parser.add_argument('-len', '--length_of_samples', help='Length of samples', type=int, default=512)
    parser.add_argument('-train', '--train', help='Train the model', nargs='?', const=True, default=False)
    parser.add_argument('-test', '--test', help='Test the model', nargs='?', const=True, default=False)
    parser.add_argument('-model_path', '--model_path', nargs = '?', help='Path to model for testing', type=str, default='False')
    args = parser.parse_args()
    return args

def run_and_save_wavelet_transform(df, device, length_of_samples=512):
    """
    This function will run the wavelet transform and save the output
    to the 'cache' folder for faster loading next time.
    :return: None
    """
    batch_size = 10
    log_eps = 1e-6
   
    my_dataset = SaveScatteringDataset(df = df, has_label=True, length_of_samples=length_of_samples)

    my_loader = DataLoader(my_dataset, batch_size=batch_size, shuffle=False, num_workers=4)
    J = 6
    Q = 16
    T = length_of_samples
    scattering = Scattering1D(J, T, Q, out_type = 'array').to(device)
    # import the lib to deepcopy the dataframe
    # make a copy of the dataframe
    # my_test_dataset = ScatteringDataset(df = df, has_label=True, do_scattering=True, normalize=False, length_of_samples=512)
    # for i in range(len(my_dataset)):
    #     # x, y = my_dataset[i]
    #     x1, Sx, y1 = my_test_dataset[i]
    #     # get next iter from loader
    #     x, y = next(iter(my_loader))
        
        
    #     print("Sx from normal: ", Sx)
    #     # get

    #     # print("x from savesxdataset: ", x[0])
        
    #     # print("x1 from normal dataset: ", x1)
    #     Sx_batch = scattering(x) # forward
    #     Sx_batch = Sx_batch[:, 1:, :]   

    #     Sx_batch = torch.log(torch.abs(Sx_batch) + log_eps) # log of absolute value of Sx
    #     Sx_batch = torch.mean(Sx_batch, dim=-1) # average over time
    #     print("Sx_batch from savesxdataset: ", Sx_batch[0])
    #     input()

    # add new columns to the dataframe Sx_0..
    Sx_batch_all = pd.DataFrame()
    for i, data in tqdm(enumerate(my_loader), total=len(my_loader)):
        # run the wavelet transform
        x_batch, y_batch = data
        x_batch = x_batch.to(device)
        
        Sx_batch = scattering(x_batch) # forward
        # from the documentation:
        # the output is in the form of a torch.Tensor or size (B, C, N1), where N1 is the signal length after subsampling to the scale (with the appropriate oversampling factor to reduce aliasing), and C is the number of scattering coefficients. 
        
        # Sx is 2d remove zeroth order as it does not contain any information
        Sx_batch = Sx_batch[:, 1:, :]   
        # print(x_batch[0])
        # print("x_batch.shape: ", x_batch[0].shape)
        # input()
        # print("Scattering:", Sx_batch[0])
        # print("Scattering shape:", Sx_batch[0].shape)
        # input()  
        # print("Sx_batch.shape: ", Sx_batch.shape)
        # for i in range(Sx_batch.shape[0]):
        #     print(Sx_batch[i])
        #     a = torch.log(torch.abs(Sx_batch[i]) + log_eps) # log of absolute value of Sx
        #     b = torch.mean(a, dim=-1) # average over time
        #     print("after mean: ", b)
        #     input()
        # input()

        
        
        
        Sx_batch = torch.log(torch.abs(Sx_batch) + log_eps) # log of absolute value of Sx
        Sx_batch = torch.mean(Sx_batch, dim=-1) # average over time
        
        # s batch as series   
        Sx_batch = pd.DataFrame(Sx_batch.cpu().numpy())
        # rename columns to Sx_0, Sx_1, Sx_2, Sx_3, Sx_4, Sx_5, Sx_6, Sx_7
        Sx_batch.columns = [f'Sx_{i}' for i in range(Sx_batch.shape[1])]
        # append after the last column to dataframe
        Sx_batch_all = pd.concat([Sx_batch_all, Sx_batch], axis=0).reset_index(drop=True)

    
    df = pd.concat([df, Sx_batch_all], axis=1)
    df.to_csv('cache/df.csv', index=False)
    # print(where nan)
    print("rown num where first col is nan:", np.where(np.isnan(df.iloc[:,0])))
    # save txt with J, Q, T
    with open('cache/params.txt', 'w') as f:
        f.write(f'J: {J}\n')
        f.write(f'Q {Q}\n')
        f.write(f'T {T}\n')

    print('Wavelet transform completed and saved to cache folder!')



def run_and_save_wavelet_transform2(df, device, length_of_samples=512):
    """
    This function will run the wavelet transform and save the output
    to the 'cache' folder for faster loading next time.
    :return: None
    """
    batch_size = 10
    log_eps = 1e-6
   
    my_dataset = SaveScatteringDatasetFull(df = df, has_label=True, length_of_samples=length_of_samples)

    my_loader = DataLoader(my_dataset, batch_size=batch_size, shuffle=False, num_workers=4)
    J = 6
    Q = 16
    T = length_of_samples
    scattering = Scattering1D(J, T, Q, out_type = 'array').to(device)


    # add new columns to the dataframe Sx_0..
    Sx_batch_all = pd.DataFrame()
    for i, data in tqdm(enumerate(my_loader), total=len(my_loader)):
        # run the wavelet transform
        x_batch, y_batch = data
        x_batch = x_batch.to(device)
        
        Sx_batch = scattering(x_batch) # forward
        # from the documentation:
        # the output is in the form of a torch.Tensor or size (B, C, N1), where N1 is the signal length after subsampling to the scale (with the appropriate oversampling factor to reduce aliasing), and C is the number of scattering coefficients. 
        
        # Sx is 2d remove zeroth order as it does not contain any information
        Sx_batch = Sx_batch[:, 1:, :]   
        # print(x_batch[0])
        # print("x_batch.shape: ", x_batch[0].shape)
        # input()
        # print("Scattering:", Sx_batch[0])
        # print("Scattering shape:", Sx_batch[0].shape)
        # input()  
        # print("Sx_batch.shape: ", Sx_batch.shape)
        # for i in range(Sx_batch.shape[0]):
        #     print(Sx_batch[i])
        #     a = torch.log(torch.abs(Sx_batch[i]) + log_eps) # log of absolute value of Sx
        #     b = torch.mean(a, dim=-1) # average over time
        #     print("after mean: ", b)
        #     input()
        # input()

        
        
        
        Sx_batch = torch.log(torch.abs(Sx_batch) + log_eps) # log of absolute value of Sx

        # flat the each item in sx_batch to 1d
        Sx_batch_arr = Sx_batch.flatten(start_dim=1)

        # Sx_batch = torch.mean(Sx_batch, dim=-1) # average over time
        
        # s batch as series   
        Sx_batch = pd.DataFrame(Sx_batch_arr.cpu().numpy())
        # rename columns to Sx_0, Sx_1, Sx_2, Sx_3, Sx_4, Sx_5, Sx_6, Sx_7
        Sx_batch.columns = [f'Sx_{i}' for i in range(Sx_batch_arr.shape[1])]
        #
        # append after the last column to dataframe
        Sx_batch_all = pd.concat([Sx_batch_all, Sx_batch], axis=0).reset_index(drop=True)

    
    df = pd.concat([df, Sx_batch_all], axis=1)
    df.to_csv('cache/df.csv', index=False)
    # save txt with J, Q, T
    with open('cache/params.txt', 'w') as f:
        f.write(f'J: {J}\n')
        f.write(f'Q {Q}\n')
        f.write(f'T {T}\n')

    print('Wavelet transform completed and saved to cache folder!')


    









def train_model(model, train_loader, val_loader, epochs, lr, device, logger, log_interval=100, save = True):
    """
    Train the model.
    :param model: model to train
    :param train_loader: train loader
    :param val_loader: validation loader
    :param epochs: number of epochs
    :param lr: learning rate
    :param device: device
    :param logger: logger - instance of metricsLogger
    :param log_interval: log interval
    :return: None
    """
    
    # optimizer
    optimizer = Adam(model.parameters(), lr=lr)
    # loss function
    criterion = nn.NLLLoss()
    # train loop
    epoch = 0
    best_epoch = 0
    not_trained = True
    best_val_acc = -float('inf')
    best_model = None
    while not_trained:
        start_time = time.time()
        correct = 0
        train_epoch_loss = 0
        val_epoch_loss = 0
        model.train()

        y_true = []
        y_pred = []

        for batch_idx, (x, Sx, y) in enumerate(train_loader):
            y_true += y.tolist()
            x = x.to(device)
            Sx = Sx.to(device)
            y = y.to(device)
            optimizer.zero_grad()
            y_hat = model(Sx)
            # print(y_hat)
            y_pred += y_hat.argmax(dim=1).tolist()
            # print(y_hat)
            # print(y_pred)
            # input()
            # print(y_pred)
            loss = criterion(y_hat, y)
            loss.backward()
            optimizer.step()
            train_epoch_loss += loss.item()
            correct += (y_hat.argmax(dim=1) == y).float().sum()
            # if batch_idx % log_interval == 0:
            #     print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
            #         epoch, batch_idx * len(Sx), len(train_loader.dataset),
            #         100. * batch_idx / len(train_loader.dataset), loss.item()))
        train_epoch_loss /= len(train_loader.dataset)
        train_epoch_acc = correct / len(train_loader.dataset)


        train_epoch_balanced_acc = balanced_accuracy_score(y_true, y_pred)

        # validate
        correct_val = 0
        model.eval()
        # y for balanced accuracy
        y_true = []
        y_pred = []
        with torch.no_grad():
            for batch_idx, (x, Sx, y) in enumerate(val_loader):
                y_true += y.tolist()
                x = x.to(device)
                Sx = Sx.to(device)
                y = y.to(device)
                y_hat = model(Sx)
                y_pred += y_hat.argmax(dim=1).tolist()
                # print(y_pred)
                # input()
                loss = criterion(y_hat, y)
                val_epoch_loss += loss.item()
                correct_val += (y_hat.argmax(dim=1) == y).float().sum()
        val_epoch_loss /= len(val_loader.dataset)
        val_epoch_acc = correct_val / len(val_loader.dataset)
        val_epoch_balanced_acc = balanced_accuracy_score(y_true, y_pred)


        print(f"Epoch {epoch+1}/{epochs}, train loss: {train_epoch_loss:.4f}, train acc: {train_epoch_acc:.4f}, val loss: {val_epoch_loss:.4f}, val acc: {val_epoch_acc:.4f}, time: {time.time() - start_time:.2f} s")
        # compute balanced accuracy of train/val set using sklearn
        print(f"Epoch {epoch+1}/{epochs}, train balanced acc: {train_epoch_balanced_acc:.4f}, val balanced acc: {val_epoch_balanced_acc:.4f}")

        logger.update(epoch, train_epoch_loss, train_epoch_acc, val_epoch_loss, val_epoch_acc, val_epoch_balanced_acc, lr)
        epoch += 1
        if val_epoch_acc > best_val_acc:
            best_val_acc = val_epoch_acc
            best_epoch = epoch
            best_model = model
        # if for 10 epochs the val acc is not better than the best val acc, stop training
        if np.abs(epoch - best_epoch) >= 10:
            not_trained = False
            print("Early stopping! The model has not improved for 10 epochs.")

        

        
    # if save, save logger and model
    if save:
        # get model name from class
        best_model_name = best_model.__class__.__name__
        # add year, month, date hour minute
        best_model_name += f'_{datetime.now().strftime("%Y%m%d_%H%M")}'
        path_of_best_model = f'models/{best_model_name}/{best_model_name}.pt'
        # path to Path
        path_of_best_model = Path(path_of_best_model)
        # create folder if not exists
        path_of_best_model.parent.mkdir(parents=True, exist_ok=True)
        # save logger
        logger.save(folder=Path(f'models/{best_model_name}'))
        # save model
        torch.save(best_model.state_dict(), path_of_best_model)
        print(f'Model saved to {best_model_name}.pt')

    print('Training completed!')
    return best_model


def test_model(model, test_loader, device):
    """
    Test the model.
    :param model: model to test
    :param test_loader: test loader
    :param device: device
    :return: None
    """
    print('Testing...')
    correct = 0
    model.eval()
    y_true = []
    y_pred = []
    with torch.no_grad():
        for batch_idx, (x, Sx, y) in tqdm(enumerate(test_loader), total=len(test_loader)):
            x = x.to(device)
            Sx = Sx.to(device)
            y = y.to(device)
            y_hat = model(Sx)
            correct += (y_hat.argmax(dim=1) == y).float().sum()
            y_true += y.tolist()
            y_pred += y_hat.argmax(dim=1).tolist()
    test_acc = correct / len(test_loader.dataset)
    print(f'Test accuracy: {test_acc:.4f}')

    return y_true, y_pred

    

class metricsLogger(): # (copied and edited from my bachelor thesis)
    """ 
    This class is used to log the metrics during training and validation
    such as train loss, train accuracy, valid loss, valid accuracy, balanced val accuracy, etc
    """
    def __init__(self, model_name):
        self.train_loss = []
        self.train_acc = []
        self.valid_loss = []
        self.valid_acc = [] 
        self.lr = []
        self.valid_balanced_acc = []
        self.epoch = []
        self.model_name = model_name

    def log_train(self, loss, acc):
        """ 
        This function is used to log the train loss and train accuracy
        """
        self.train_loss.append(loss)
        self.train_acc.append(acc)

    def log_valid(self, loss, acc, balanced_acc):
        """
        This function is used to log the valid loss, valid accuracy and balanced valid accuracy
        """        
        self.valid_loss.append(loss)
        self.valid_acc.append(acc)
        self.valid_balanced_acc.append(balanced_acc)

    def log_lr(self, lr):
        """
        This function is used to log the learning rate
        """
        self.lr.append(lr)
    
    def log_epoch(self, epoch):
        """
        This function is used to log the epoch
        """
        self.epoch.append(epoch)

    def save_confusion_matrix(self, y_true, y_pred, labels: dict , folder:Path = Path('temp_metrics')):
        """
        This function is used to save the confusion matrix
        """
        # map the labels to the y_true and y_pred
        y_true = [list(labels.keys())[i] for i in y_true]
        y_pred = [list(labels.keys())[i] for i in y_pred]
        
        
        labels_list = list(labels.keys())
        print(labels_list)

        cm = confusion_matrix(y_true, y_pred, labels=labels_list)
        print(cm)
        plt.figure(figsize=(8, 6))
        sns.set(font_scale=1) # Adjust to fit
        sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', cbar=False, 
                    xticklabels=labels_list, yticklabels=labels_list)
        plt.xlabel('Predicted labels')
        plt.ylabel('True labels')
        # plt.title('Confusion matrix')
        plt.savefig(f'{folder}/confusion_matrix.pdf')
        plt.close()
        

    def update(self, epoch, train_loss, train_acc, valid_loss, valid_acc, valid_balanced_acc, lr):
        """
        This function is used to update all the metrics
        - epoch, train loss, train acc, valid loss, valid acc, valid balanced acc, lr
        """
        self.log_epoch(epoch)
        self.log_train(train_loss, train_acc)
        self.log_valid(valid_loss, valid_acc, valid_balanced_acc)
        self.log_lr(lr)
    

    def save(self, folder:Path = Path('metrics')):
        """
        This function is used to save the metrics in a csv file
        the csv file will be saved in the current directory
        with the name in format <model_name>_metrics.csv
        """
        df = pd.DataFrame({ 'epoch': self.epoch,
                            'train_loss': self.train_loss,
                           'train_acc': self.train_acc,
                           'valid_loss': self.valid_loss,
                           'valid_acc': self.valid_acc,
                           'valid_balanced_acc': self.valid_balanced_acc,
                           'lr': self.lr
                           })
        path_to_csv = folder / f'{self.model_name}_metrics.csv'
        path_to_csv = Path(path_to_csv)
        df.to_csv(path_to_csv, index=False)
        print(f'Metrics saved to {path_to_csv}')


if __name__ == '__main__':

    T = 512 # this is the length of the signal
    J = 8 # maximum scale of the scattering transform 
    Q = 12 # number of wavelets per octave

    log_eps = 1e-6

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    torch.manual_seed(42)

    x_all = torch.zeros(len(files), T, dtype=torch.float32, device=device)
    print(x_all.shape)

    input()
    y_all = torch.zeros(len(files), dtype=torch.int64, device=device)
    subset = torch.zeros(len(files), dtype=torch.int64, device=device)

    for k, f in enumerate(files):
        basename = f.split('.')[0]

        # Get label (0-9) of recording.
        y = int(basename.split('_')[0])

        # Index larger than 5 gets assigned to training set.
        if int(basename.split('_')[2]) >= 5:
            subset[k] = 0
        else:
            subset[k] = 1

        # Load the audio signal and normalize it.
        _, x = wavfile.read(os.path.join(path_dataset, f))
        x = np.asarray(x, dtype='float')
        x /= np.max(np.abs(x))

        # Convert from NumPy array to PyTorch Tensor.
        x = torch.from_numpy(x).to(device)

        # If it's too long, truncate it.
        if x.numel() > T:
            x = x[:T]

        # If it's too short, zero-pad it.
        start = (T - x.numel()) // 2

        x_all[k,start:start + x.numel()] = x
        y_all[k] = y



    scattering = Scattering1D(J, T, Q).to(device)
    Sx_all = scattering.forward(x_all)

    Sx_all = Sx_all[:,1:,:]

    Sx_all = torch.log(torch.abs(Sx_all) + log_eps)

    Sx_all = torch.mean(Sx_all, dim=-1)