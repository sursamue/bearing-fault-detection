import pandas as pd
import argparse
import numpy as np
import matplotlib.pyplot as plt
from torchview import draw_graph
import models as models
import graphviz 
import torch
import subprocess
import os
import torch.nn as nn
from pathlib import Path



# parse arguments
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-df", "--dataframe", type=Path, const=True, nargs='?', default = False, help="path to metrics dataframe")
    parser.add_argument("-net", "--net", const=True,  nargs='?', default = False, help="plot net")
    parser.add_argument("-hist", "--histogram", const=True,  nargs='?', default = False, help="plot histogram")
    args = parser.parse_args()
    return args

def plot_metrics(df):
    # Assuming 'train_acc' and 'valid_acc' columns exist in the dataframe
    # train_acc = [tensor.item() for tensor in df['train_acc'].values]
    # valid_acc = [tensor.item() for tensor in df['valid_acc'].values]
    train_acc =  df['train_acc'].str.extract('([0-9\.]+)').astype(float).values
    valid_acc = df['valid_acc'].str.extract('([0-9\.]+)').astype(float).values
    
    print(train_acc)
    epochs = range(1, len(train_acc) + 1) 
    # plot vertical line at 33 epoch
    plt.axvline(x=33, color='black', linestyle='--')
    

    plt.plot(epochs, train_acc, label='Train Accuracy')
    plt.plot(epochs, valid_acc, label='Valid Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    # plt.title('Train and Valid Accuracy per Epoch')
    # add one more tick to x axis at 33
    plt.xticks(list(plt.xticks()[0]) + [33])
    plt.legend()
    # grid
    plt.grid(True)
    # save plot as pdf
    plt.xlim([1, 43])
    plt.savefig('tmp/accuracy.pdf')
    # plt.savefig('plots/accuracy.png')
    
    plt.show()


def plot_net():
    # graphviz.set_jupyter_format('png')
    # model = models.Net8(input_size=221, output_size=4)
    # modify net, so it will not have self.double()

    model_graph = draw_graph(models.Net8(input_size=221, output_size=4), input_size=(32,221,16), graph_name='Net8', 
                             hide_inner_tensors=True, hide_module_functions=True, save_graph = True, graph_dir = 'TB', expand_nested=True, depth = 1)
    model_graph.resize_graph(scale=5.0)
    model_graph.visual_graph.render(format='pdf')
    
    # def convert_svg_to_pdf(svg_file, pdf_file):
    #     try:
    #         os.system(f'inkscape -z -f {svg_file} -A {pdf_file}')
    #         print("Conversion successful!")
    #     except FileNotFoundError:
    #         print("Inkscape is not installed or not found in the system.")

    # # Specify the paths to the SVG and PDF files
    # svg_file = "Net8.gv.svg"
    # pdf_file = "Net8.gv.pdf"

    # # Call the function to convert SVG to PDF
    # convert_svg_to_pdf(svg_file, pdf_file)

def plot_histogram(df_path, verbose = True, not_cropped=False):
    """This function will plot histogram of the classes in the dataset"""
    # get the num of classes
    df = pd.read_csv(df_path)
    num_classes = df['label'].value_counts()
    # get num classes's label but only with unique filenames
    if not_cropped:
        num_classes = df.drop_duplicates(subset=['filename'])
        # get num of classes
        num_classes = num_classes['label'].value_counts()
    # sort by values
    num_classes = num_classes.sort_index()
    # map num classes to {'N':0, 'B':1, 'IR':2, 'OR':3}
    dict_of_classes = {'N':0, 'B':1, 'IR':2, 'OR':3}
    # switch keys and values
    dict_of_classes = dict(zip(dict_of_classes.values(), dict_of_classes.keys()))
    num_classes = num_classes.rename(index=dict_of_classes)
    if verbose:
        print(num_classes)

    # plot histogram
    plt.bar(num_classes.index, num_classes.values)
    plt.xlabel('Classes')
    plt.ylabel('Number of samples')
    # grid
    plt.grid(True)
    # plt.title('Histogram of classes')
    # put yticks where the histogram values are
    print(num_classes.values)
    # put num_classes.values as above the each bar
    for index, value in enumerate(num_classes.values):
        plt.text(index, value, str(value), ha = 'center')#, fontweight = 'bold')

    plt.savefig('tmp/histogram.pdf')
    plt.show()
