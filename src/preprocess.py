from preprocess_utils import *
import warnings
import os

import torch
# import torch.nn as nn
import kymatio
# from kymatio.torch import Scattering1D
import numpy as np
from kymatio.numpy import Scattering1D
from data_urls import URLS

def plot_signal_from_df(df: pd.DataFrame, path_to_file: Path, segment_number: int):
    '''
    Plot the signal from the DataFrame returned by matfile_to_df()
    input:
        df: DataFrame returned by matfile_to_df()
        path_to_file: Path to the file which contains the signal
        segment_number: Segment number to plot
    '''
    # each row represents a segment, the name of the columns are just the number 0 - 511
    # the values are the signal values
    segment = df.iloc[segment_number, 2:].values
    # plot the signal
    plt.plot(segment)
    # the name of the window is the filename + segment number
    # filename is just the last part of the path
    plt.title(path_to_file.name + ', segment ' + str(segment_number))
    plt.show()



def main():
    
    # data path
    # path = Path('../data')
    # #
    # #print(URLS['DE_12k'])
    # #URLS['DE_12k'] is a dict of urls with name as a key and url as a value

    for name, url in URLS['DE_12k'].items():
        # print(url, path, name)
        # rewrite url from old format "http://csegroups.case.edu/sites/default/files/bearingdatacenter/files/Datafiles/235.mat" to new format 
        # https://engineering.case.edu/sites/default/files/105.mat
        url = url.replace('http://csegroups.case.edu/sites/default/files/bearingdatacenter/files/Datafiles/', 'https://engineering.case.edu/sites/default/files/')

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

            download(url, path, name, suffix='.mat')
            # check file size, if less than 30 KiB yield warning
            if os.path.getsize(path / (name + '.mat')) < 30 * 1024:
                print("Warning: {} is less than 30 KiB".format(name))
    

    folder_path = Path('../data')

    
    df = get_df_all(folder_path, segment_length=1024)
    # # save df to csv
    df.to_csv('df_1024.csv', index=False)
    print("DataFrame saved to csv!")
    # read df from csv
    # df = pd.read_csv('df_1024.csv')

    # path to sample file
    # get name of first sample
    # sample_path = df['filename'][0]
    # path_to_sample = Path(sample_path)
    # # plot sample

    # # plot_signal_from_df(df, path_to_sample, 0)
    # # # # plot_signal_from_df(df, path_to_sample, 1)
    # # # # plot_signal_from_df(df, path_to_sample, 2)

    # # # # get first sample
    # sample = df.iloc[0, 2:1026].values 
    # sample = sample.astype(np.float32)


    # sample = sample/np.max(sample)
    # #
    # # print(sample.shape)
    # # print(sample)

    # # # # #plot sample
    # # plt.figure(figsize=(8, 3))
    # # plt.plot(sample)
    # # plt.grid()
    # # # axis names
    # # plt.xlabel('Sample points')
    # # plt.ylabel('Amplitude')
    # # # save plot
    # # plt.savefig('sample.pdf')
    # # plt.tight_layout()  # <- HERE
    # # plt.show()

    # # # # sample to float
    # # sample = sample.astype(np.float32)


    # # sample = sample/np.max(sample)
    # # # # plt.plot(sample)
    # # # # plt.show()

    # # # print(sample)

    # # # input()
    # # # # sample = np.array([sample])
    # # # # sample = sample.contiguous()

    # # print(sample.shape)
    # T = 1024
    # J = 6
    # Q = 16

    # scattering = Scattering1D(J, T, Q)
    # Sx = scattering(sample)
    # # print(Sx.shape)
    # # input()
   

    # meta = scattering.meta()
    # print(np.where(meta['order'] == 6))
    # order0 = np.where(meta['order'] == 0)
    # order1 = np.where(meta['order'] == 1)
    # order2 = np.where(meta['order'] == 2)

    # # plt.figure(figsize=(8, 2))
    # # plt.plot(sample)
    # # plt.grid()
    # # # plt.title('Original signal')


    # # ZEROTH

    # plt.figure(figsize=(8, 8))


    # plt.subplot(3, 1, 1)
    # plt.ylabel('Values')
    # plt.xlabel("Columns")
    # plt.plot(Sx[order0][0])
    # plt.grid()
    # plt.title('Zeroth-order scattering')

    # # FIRST
    # plt.subplot(3, 1, 2)
    # plt.imshow(Sx[order1], aspect='auto')
    # plt.ylabel('Rows')
    # plt.xlabel("Columns")
    # plt.title('First-order scattering')

    # # SECOND
    # plt.subplot(3, 1, 3)
    # plt.imshow(Sx[order2], aspect='auto')
    # plt.ylabel('Rows')
    # plt.xlabel("Columns")
    # plt.title('Second-order scattering')

    # plt.tight_layout()
    # plt.show()


   
  


main()    
