# Bearing Fault Detection
Welcome to Bearing Fault Detection semestral work from the course 
Diagnostics and Testing at the Czech Technical University in Prague, Faculty of Electrical Engineering.

# Dependencies
The program is written in Python 3.10.9. The dependencies are listed in the `requirements.txt` file.
To install the dependencies please run
```bash
pip3 install -r requirements.txt
```

<!-- # Run
To train and test the neural network use 
```
python3 main.py
```
To recalculate mean, std, and wavelet transform. Please run 
```
python3 main.py -pl
```
This will firstly purge all of the cached data and then create new data. -->

# Download CWRU dataset
To download the CWRU dataset please run
```bash
python3 preprocess.py
```
This will download DE_12k from the CWRU website to the `data` folder. Then it will extract the data from the `data` folder to the pandas dataframe called 
`df_1024.csv`. The data will be cut into 1024 long samples. This may be manually 
changed in the `preprocess.py` file.

Code related to preprocessing is located in the `preprocess.py` and `preprocess_utils.py` files.

# Run
The main program has couple of modes. To see the help please run
```bash
python3 main.py -h
```
We shall describe the most important modes here. Please run the program 
with the paramaters as specified in the following sections. 

## Purge and load
The program store it's important data in the `cache` folder. The user may want 
to purge and reload the data. This can be done by running
```bash
python3 main.py -pl2 -len 1024 -full
```
Please, run this also for the first time you run the program.

It will generate the wavelet scattering transform of the data and store it in the `cache` folder.

The `-len` parameter specifies the length of the samples. It is optional, however,
the default is set to 512. It should be set to the predefined length of the samples
in the `preprocess.py` file.

The `-full` parameter specifies whether to use the full images or not. It is optional, however, currently you have to specify it.

After the model is trained, it is stored in the `models` folder with the 
log file.


## Train
To train the model please run
```bash
python3 main.py -train -len 1024 -full
```
## Test
To test the model please run
```bash
python3 main.py -test -len 1024 -full --model_path <path_to_model>
```

The `--model_path` parameter specifies the path to the model. 
Currently only one type of architecture is supported and that is `Net8` as 
specified in the `models.py` file. 

After the model is tested, the confusion matrix is stored.

# Structure
The program is structured as follows:

Files:
- `main.py` - main file
- `utils.py` - contains the functions used in the `main.py` file
- `models.py` - contains the architecture of the neural network
- `preprocess.py` - contains the code for downloading and preprocessing the data
- `preprocess_utils.py` - contains the functions used in the `preprocess.py` file
- `data_url.py` - contains the url to the CWRU dataset
- `plot.py` - contains the code for plotting the data
- `plot_utils.py` - contains the functions for plotting the data

Folders:
- `cache` - contains the cached data
- `data` - contains the downloaded data
- `models` - contains the trained models


# Remarks
The program has been tested on CPU. It should support GPU, however, it has not been tested.
The proposed system has been presented in front of the teachers at the CTU in Prague, Faculty of Electrical Engineering. The presentation is located in the `presentation` folder.
Report is located in the `report` folder.



# License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.








<!-- usage: main.py [-h] [-p [PURGE]] [-l [LOAD]] [-pl [PURGE_LOAD]] [-pl2 [PURGE_LOAD2]] [-full [FULL]] [-len LENGTH_OF_SAMPLES] [-train [TRAIN]] [-test [TEST]]
               [-model_path [MODEL_PATH]]

Bearing fault detection. Example usage: $ python3 main.py -full -len 1024 -test -model models/Net8_20240103_0025/Net8_20240103_0025.pt

options:
  -h, --help            show this help message and exit
  -p [PURGE], --purge [PURGE]
                        Will delete the temp folder and run program
  -l [LOAD], --load [LOAD]
                        Load the dishwas...Do the scattering transform and save it to the dataframe
  -pl [PURGE_LOAD], --purge_load [PURGE_LOAD]
                        Purge the cache and load the dishwas...Do the scattering transform and save it to the dataframe
  -pl2 [PURGE_LOAD2], --purge_load2 [PURGE_LOAD2]
                        Purge the cache and load the dishwas...Do the scattering transform and save it to the dataframe
  -full [FULL], --full [FULL]
                        Use full images
  -len LENGTH_OF_SAMPLES, --length_of_samples LENGTH_OF_SAMPLES
                        Length of samples
  -train [TRAIN], --train [TRAIN]
                        Train the model
  -test [TEST], --test [TEST]
                        Test the model
  -model_path [MODEL_PATH], --model_path [MODEL_PATH]
                        Path to model for testing -->