\documentclass[twoside]{article}
\usepackage[a4paper]{geometry}
\geometry{verbose,tmargin=2.5cm,bmargin=2cm,lmargin=2cm,rmargin=2cm}
\usepackage{fancyhdr}
\pagestyle{fancy}

\usepackage{amsfonts} 
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Matlab,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}


\usepackage{gensymb}
\usepackage{breqn}
% nastaven
\usepackage{lmodern}
\usepackage[utf8]{inputenc}
% \usepackage[slovak]{babel}
\usepackage[T1]{fontenc}

\usepackage{enumitem}
% odkazy
\usepackage{url}

\usepackage{bbm}
\usepackage{pdflscape}
% vcesloupcov tabulky
\usepackage{multirow}

\newtheorem{question}{Question}


% vnoen popisky obrzk
\usepackage{subcaption}

% automatick konverze EPS 
\usepackage{graphicx} 
\usepackage{epstopdf}
\usepackage{float}
\usepackage{amsmath}

% odkazy a
\usepackage[unicode=true, bookmarks=true,bookmarksnumbered=true,
bookmarksopen=false, breaklinks=false,pdfborder={0 0 0},
pdfpagemode=UseNone,backref=false,colorlinks=true] {hyperref}



% Poz
\usepackage{xkeyval}	% Inline todonotes
\usepackage[textsize = footnotesize]{todonotes}
\presetkeys{todonotes}{inline}{}

% Zacni sekci slovem ukol
\renewcommand{\thesection}{\arabic{section}}
% enumerate zacina s pismenem
%\renewcommand{\theenumi}{\alph{enumi}}

% smaz aktualni page layout
\fancyhf{}
% zahlavi
\usepackage{titling}
\fancyhf[HC]{\thetitle}
\fancyhf[HLE,HRO]{\theauthor}
\fancyhf[HRE,HLO]{\today}
 %zapati
\fancyhf[FLE,FRO]{\thepage}
\usepackage[style=ieee]{biblatex}
\addbibresource{ref.bib}

% daje o autorovi
\title{Automated system for bearing fault detection from vibration signals}
\author{Samuel Šúr}
\date{\today}
\newcounter{countedItem}
\newenvironment{countedItemize}{\begin{list}{\arabic{countedItem}.}{\usecounter{countedItem}}}{\end{list}}

\begin{document}
\maketitle

%Detekce poruch ložisek pomocí metody wavelet scattering
%        Navrhněte, implementujte a vyhodnoťte funkci automatizovaného systému 
%        pro detekci poruchy ložiska z vibračních signálů (obdržíte jako 
%        dataset). Použijte wavelet scattering transform/network v Pythonu.

\section{Problem definition}
\label{sec:problem_definition}
The goal of this work is to design, implement and evaluate an automated 
system for bearing fault detection from vibration signals. The system will be 
based on wavelet scattering transform/network in Python. 



\section{State-of-the-art}
\label{sec:state_of_the_art}
Data driven methods for fault detection 
involve the use of machine learning methods to detect the fault.
M. Mohiuddin \cite{mohiuddin_rolling_2022} used a method, where 
the vibration signal is decomposed using the wavelet transform.
Then the 2D image is created from the decomposed signal. The 
image is then used as an input to the convolutional neural network.
The convolutional neural network is used to classify the type of the fault.
This work is later expanded by M. Mohiuddin et al. \cite{mohiuddin_intelligent_2023} using the state-of-the-art neural network architectures.


X. Wang et al. \cite{wang_bearing_2015} used feature extraction using time-domain, 
frequency-domain and time-frequency domain analysis. This was followed by 
dimensionality reduction followed by Support vector machine (SVM) classifier.


Another method, proposed by A. Mosallam et al. \cite{mosallam_nonparametric_2013}
uses feature extraction followed by feature compression 
using the principal component analysis. 
The EMD algorithm is used to decompose the signal into
intrinsic mode functions. However, this method does not output the type of the fault,
but rather the health state of the bearing (prediction of the remaining useful life (RUL) of the bearing).



% Another approach, by F. Sloukia et al. \cite{sloukia_bearings_nodate}
% uses Gaussian Hidden Markov model accompanied by the SVM classifier. It 
% predicts the Remaining Useful Life (RUL) of the bearing.

% H. Li et al. \cite{li_rolling_nodate} uses feature 
% extraction (such as rms, wavelet entropy,...), followed
% by logistic regression model to predict the RUL of the bearing.

\section{Proposed methods}
\label{sec:proposed_methods}
The proposed dataset for this work has been created by Case Western Reserve University (CWRU) Bearing Data Center~\footnote{\url{https://engineering.case.edu/bearingdatacenter/download-data-file}}.
The dataset contains vibration signals with faults and without faults.
The data with faults is further divided depending on the type of the fault: inner, outer and ball fault.

% This work will use the dataset created by P. Nectoux et al. \cite{nectoux_pronostia_2012}.
% The dataset has been created fro PHM 2012 Data Challenge.
% The dataset contains of run-to-failure vibration signals read by two accelerometers and 
% temperature data read by PT100 sensor. The dataset does not contain any
% information regarding the type of the failure. Therefore the 
% exact type of the failure cannot be determined, however the 
% health state and the time to the failure can be estimated.
Therefore the proposed method is to use wavelet scattering transform.
The wavelet scattering transform extracts features from the signal using 
the series of wavelet transforms. The wavelet transform is a convolution of
the signal with a wavelet function. This can be performed 
using the Python library called \textit{Kymatio} \footnote{\url{https://www.kymat.io/}}.
There are other alternative libraries such as \textit{pywt}~\footnote{\url{https://github.com/PyWavelets/pywt}}, 
or \textit{ssqueezepy}~\footnote{\url{https://github.com/OverLordGoldDragon/ssqueezepy}}.

After the features have been extracted, we can use classifier methods (eg. kNN, convolutional neural network, SVM,...)
to classify the signal into one of the classes (healthy, inner fault, outer fault, ball fault).

\section{Evaluation}
\label{sec:evaluation}
The CWRU dataset shall be divided into 3 parts: training, validation and testing sets.
I will 
use the testing data for evaluation of the proposed method.






%Run-to-Failure Data: If run-to-failure data of many machines exists, then health state of a particular machine can be estimated by comparing the others.

\printbibliography


\end{document}